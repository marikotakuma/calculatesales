package jp.alhinc.mariko_takuma.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		// 条件ファイルのdefinitionMapを作成
		Map<String, String> definitionMap = new HashMap<String, String>();

		// 売上合計のdefinitionMapを作成
		Map<String, Long> totalSalesMap = new HashMap<String, Long>();

		//System.out.println("ここにあるファイルを開きます=>" + args[0]);



		//支店定義ファイルメソッドに引数を渡している
		if(!definitionFileInput(args[0], "branch.lst", definitionMap, totalSalesMap, "[0-9]{3}", "支店")) {
			return;
		}

		// Fileクラスのdirにパスを入れる
		File dir = new File(args[0]);

		// File配列listにファイルパスを入れている
		File[] list = dir.listFiles();

		//指定した条件のファイルのみを入れるリストをつくる
		List<String> matchList = new ArrayList<String>();


		//条件指定して配列のサイズの数だけループする
		for(int i = 0; i < list.length; i++) {

			//リストの中の指定した条件のファイルを検索
			if(list[i].isFile() && list[i].getName().matches("[0-9]{8}.rcd")) {

				//検索した条件のファイルをリストに入れる
				matchList.add(list[i].getName());
			}
		}
		for(int i = 1; i < matchList.size(); i++) {

			//リストから数字だけの文字列を取り出している
			String before = matchList.get(i - 1).substring(0, 8);
			String  after = matchList.get(i).substring(0, 8);

			//String型をint方にキャストしている
			int n = Integer.parseInt(before);
			int a = Integer.parseInt(after);

			if(!((a - n) == 1)) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//条件指定してリストの数だけループする
		for(int i = 0; i < matchList.size(); ++i) {

			//売り上げファイルメソッドに引数を渡している
			if(!salesFileInput(args[0], matchList.get(i), totalSalesMap)) {
				return;
			}
		}

		//

		//集計の出力メソッドに引数を渡してる
		if(!aggregateOutput(args[0], "branch.out", definitionMap, totalSalesMap )) {
			return;
		}
	}

	//支店定義ファイルの読み込みメソッド

	private static boolean definitionFileInput(String filePath, String fileName, Map<String, String> definitionMap, Map<String, Long> totalSalesMap, String conditions, String name
) {

		// FileReaderをまとめて処理するため
		BufferedReader br = null;
		try {
			// banch.listの中身を読み込む
			File file = new File (filePath, fileName);

			//支援定義ファイルが存在するかどうか
			if(!file.exists()) {
				System.out.println(name + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);

			// BufferdReaderの変数brに入れる
			br = new BufferedReader(fr);

			// String型の変数指定
			String Line;

			// ファイルの中がnullになるまで読み込む
			while((Line = br.readLine() ) != null) {

				// ファイルの中をカンマで区切って配列に入れる
				String[] definitio = Line.split(",");

				//支店定義ファイルのフォーマットが指定した条件のファイル以外の場合
				////支店定義ファイルの要素数が１以下３以上の場合
				if(!definitio[0].matches(conditions) || definitio.length != 2) {
					System.out.println(name + "定義ファイルのフォーマットが不正です");
					return false;
				}

				//System.out.println(definitio[0] + definitio[1]);

				// definitionMapの中にkeyを支店コード、値を支店コードとして入れる
				definitionMap.put(definitio[0], definitio[1]);

				// definitionMapに支店コードと値(int型からLong型に変換している)を入れる
				totalSalesMap.put(definitio[0], 0L);
			}
			// keyを指定して中身を確認
			//System.out.println(definitionMap.get("001"));
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br !=null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//売り上げファイル読み込みのメソッド

	private static boolean salesFileInput(String filePath, String fileName, Map<String, Long> totalSalesMap) {



		//System.out.println(list[i].getName());

		//売り上げファイルを入れるArrayListを作成する
		List<String> salesList = new ArrayList<String>();

		BufferedReader br = null;

		try{
			File salesFile = new File(filePath, fileName);

			// FileReaderで配列listの中身を読み込んでsalesFailに入れる
			FileReader fr = new FileReader(salesFile);

			// salesFailをbrに入れる
			br = new BufferedReader(fr);

			String salesLine;


			// // ファイルの中がnullになるまで読み込む
			while((salesLine = br.readLine() ) != null){

				if(!salesLine.matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				// arrayListの中売り上げファイルの中身を追加する
				salesList.add(salesLine);

				//System.out.println(salesList);
			}

			//支店に該当がなかった場合

			//Mapの中にkeyが含まれているかどうか検索
			if(!(totalSalesMap.containsKey(salesList.get(0)))) {
				System.out.println(salesFile.getName() + "の支店コードが不正です");
				return false;
			}

			//売上ファイルの中身が３以上ある場合のエラー

			//読み込んだ行の数が2じゃない場合
			if(salesList.size() != 2) {
				System.out.println(salesFile.getName() + "のフォーマットが不正です");
				return false;
			}


			// Long型の変数にarrayListの値を入れる(Long型にキャストしてる)
			Long newsales = Long.parseLong(salesList.get(1));

			// Long型の変数にlistから取り出したkeyに該当する値を入れている
			Long beforSalse = totalSalesMap.get(salesList.get(0));


			// 加算した数値を出力
			//System.out.println(newsales + beforSalse);

			// Long型の変数に加算した数値をいれる
			Long newBeforSalse = newsales + beforSalse;

			//加算した数値int型をString型に変える
			String sum = String.valueOf(newBeforSalse);

			//文字列が10より大きいときに出力
			if(sum.length() > 10) {
				System.out.println("合計金額が10桁を超えました");
				return false;
			}
			// 加算した数値を入れた変数をmapに入れる
			totalSalesMap.put(salesList.get(0), newBeforSalse);

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try{
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}

		return true;
	}

	//出力のメソッド

	//戻り値を設定、引数（Mapの引数×２とコマンドライン引数）をmainメソッドからもらっている。
	private static boolean aggregateOutput(String filePath, String fileName, Map<String, String> definitionMap, Map<String, Long> totalSalesMap) {
		BufferedWriter bw = null;
		try{
			//フォルダのなかのbranch.outを読み込み
			File bdfile = new File(filePath, fileName);

			//読み込んだ内容を変数fwにいれる
			FileWriter fw = new FileWriter(bdfile);

			//
			bw = new BufferedWriter(fw);

			//拡張for文でmapのすべてのkeyを取得
			for(String key :definitionMap.keySet()) {
				//System.out.println(key + "," + definitionMap.get(key) + "," + totalSalesMap.get(key));

				//ファイルに取得したkeyを使い書き込む
				bw.write(key + "," + definitionMap.get(key) + "," + totalSalesMap.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null) {
				try{
					bw.close();
				}catch(Exception e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}

		//結果trueをmainに戻している
		return true;
	}
}
